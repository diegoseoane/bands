const express = require("express");
const dotenv = require("dotenv");
const { connect } = require("./database/index.js");
const { bandRouter } = require("./routers/bands.js");
const { albumRouter } = require("./routers/albums.js");
const { songRouter } = require("./routers/songs.js");
const { userRouter } = require("./routers/users.js");

async function main() {
    
    dotenv.config();
    const app = express();
    app.use(express.json());

    const PORT = process.env.PORT;
    const DB_PORT = process.env.DB_PORT;
    const DB_USERNAME = process.env.DB_USERNAME;
    const DB_PASSWORD = process.env.DB_PASSWORD;
    const DB_HOST = process.env.DB_HOST;
    const DB_DATABASE = process.env.DB_DATABASE;

    try {
        await connect(DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        app.use("/", bandRouter);
        app.use("/", albumRouter);
        app.use("/", songRouter);
        app.use("/", userRouter)
        app.listen(PORT, () => {
            console.log('Server is running...');
        })
    } catch (error) {
        console.log("No pudo cargar la base de datos", error);
    }

}

main();