const { DataTypes } = require('sequelize');

function createSongModel(connection) {
  const Song = connection.define('Song', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT
    },
    yearOfCreation:{
      type: DataTypes.DATE
    }
  })
  return Song;
}

module.exports = {
  createSongModel
}