const { DataTypes } = require('sequelize');

function createAlbumModel(connection) {
  const Album = connection.define('Album', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT
    },
    yearOfCreation:{
      type: DataTypes.DATE
    },
  })
  return Album;
}

module.exports = {
  createAlbumModel
}