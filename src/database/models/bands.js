const { DataTypes } = require('sequelize');

function createBandModel(connection) {
  const Band = connection.define('Band', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT
    },
    yearOfCreation:{
      type: DataTypes.DATE
    }
  })
  return Band;
}

module.exports = {
  createBandModel
}