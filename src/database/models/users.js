const { DataTypes, Model } = require("sequelize");

function createUserModel(connection) {
    const User = connection.define('User', {
        username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING
        }
    })
    return User
}

module.exports = {
    createUserModel
}