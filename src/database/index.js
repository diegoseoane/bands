const Sequelize = require("sequelize");
const { createBandModel } = require("./models/bands.js");
const { createAlbumModel } = require("./models/albums.js");
const { createSongModel } = require("./models/songs.js");
const { createUserModel } = require("./models/users.js");

const models = {};

async function connect(host, port, username, password, database) {
    
    const sequelize = new Sequelize({
        database,
        username,
        password,
        host,
        port,
        dialect: 'mariadb',
    });
    
    models.Band = createBandModel(sequelize);
    models.Album = createAlbumModel(sequelize);
    models.Song = createSongModel(sequelize);
    models.User = createUserModel(sequelize)

    models.Band.hasMany(models.Album);
    models.Album.belongsTo(models.Band);

    models.Album.hasMany(models.Song);
    models.Song.belongsTo(models.Album);

    models.Band.hasMany(models.Song);
    models.Song.belongsTo(models.Band);
/*
    models.User.hasMany(models.Song);
    models.Song.belongsToMany(models.User, { through: 'likedSongs'});

    models.User.hasMany(models.Album);
    models.Album.belongsToMany(models.User, { through: 'likedAlbums'});

    models.User.hasMany(models.Band);
    models.Band.belongsToMany(models.User, { through: 'likedBands'});
*/
    try {
        await sequelize.authenticate();
        await sequelize.sync();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

function getModel(name) {
    if (models[name]) {
        return models[name];
    } else {
        console.error(`Model ${name} does not exists.`);
        return null;
    }
}

module.exports = {
    connect,
    getModel
};