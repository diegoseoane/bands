const express = require("express");
const { getModel } = require("../database/index.js");
const songRouter = express();

songRouter.get("/songs", async (req, res) => {
    try {
        const s = await getModel('Song').findAll();
        res.status(200).json(s);
    } catch(error) {
        console.log(error);
        res.status(404).json(`No hay canciones`);
    }
})

songRouter.get("/songs/:id", async (req, res) => {
    try {
        const Album = await getModel('Album');
        const Band = await getModel('Band');
        const b = await getModel('Song').findOne({
            where: {
                id: req.params.id
            },
            include: [Album, Band]
        });
        res.status(200).json(b);
    } catch {
        res.status(404).json(`No hay canciones`);
    }
})

songRouter.post("/songs", async (req, res) => {
    try {
        const Song = getModel('Song');
        const b = new Song({
            name: req.body.name,
            description: req.body.description,
            yearOfCreation: req.body.yearOfCreation
        })
        await b.save();
        return res.status(200).json(`La canción ${b.name} ha sigo guardada`);
    } catch {
        res.status(404).json(`La canción no ha podido ser cargada`);
    }
})

songRouter.put("/songs/:id", async (req, res) => {
    try {
        const b = await getModel('Song').findOne({
            where: {
                id: req.params.id
            }
        });
        b.description = req.body.description;
        b.save();
        res.status(200).json(b);
    } catch {
        res.status(404).json(`No hay canciones`);
    }
})

songRouter.delete("/songs/:id", async (req, res) => {
    try {
        const b = await getModel('Song').findOne({
            where: {
                id: req.params.id
            }
        });
        b.destroy();
        res.status(200).json(`La cancíón ha sido eliminada` );
    } catch {
        res.status(404).json(`No hay canciones`);
    }
})

module.exports = {
    songRouter
}