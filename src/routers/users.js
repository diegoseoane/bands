const express = require("express");
const { getModel } = require("../database");
const userRouter = express();

userRouter.post("/users", async (req, res) => {
    try {
        const User = getModel('User');
        const u = new User({
            username: req.body.username,
            password: req.body.password
        })
        u.save()
        res.status(200).json(`El usuario ${u.username} ha sido agregado con éxito`);
    } catch(error) {
        res.status(404).json(`Ha habido un error`)
    }
})

userRouter.get("/users", async (req, res) => {
    try {
        const User = getModel('User');
        const u = await User.findAll();
        res.status(200).json(u);
    } catch(error) {
        res.status(404).json(`Ha habido un error`)
    }
})
/*
userRouter.post("/likedsongs/:idUser/:idSong", async (req, res) => {
    try {
        const s = await getModel('Song').findOne({
            where: {
                id: req.params.idSong
            },
        })        
        const u = await getModel('User').findOne({
            where: {
                id: req.params.idUser
            },
        })
        //s.Userid.push(req.params.idUser) ;
        s.save();
        res.status(200).json(`${s.name} ha sido agregada a la lista de favoritos de ${u.username}`);
    } catch(error) {
        res.status(404).json(`Ha habido un error`)
    }
})

userRouter.get("/liked/:id", async (req, res) => {
    try {
        const l = await getModel('Song').findAll({
            where: {
                Userid: req.params.id
            }
        })
        const a = await getModel('Album').findAll({
            where: {
                Userid: req.params.id
            }
        })
        const b = await getModel('Band').findAll({
            where: {
                Userid: req.params.id
            }
        })
        let liked = [l, a, b];
        res.status(200).json(liked)
    } catch(error) {
        res.status(400).json("error")
    }
})
*/
module.exports = {
    userRouter
}