const express = require("express");
const { getModel } = require("../database/index.js");
const albumRouter = express();

albumRouter.get("/albums", async (req, res) => {
    try {
        
        const a = await getModel('Album').findAll();
        res.status(200).json(a);
    } catch {
        res.status(404).json(`No hay albumes`);
    }
})

albumRouter.get("/albums/:id", async (req, res) => {
    try {
        const Song = getModel('Song');
        const a = await getModel('Album').findOne({
            where: {
                id: req.params.id
            },
            include: Song
        });
        res.status(200).json(a);
    } catch {
        res.status(404).json(`No hay albumes`);
    }
})

albumRouter.post("/albums", async (req, res) => {
    try {
        const Album = getModel('Album');
        const a = new Album({
            name: req.body.name,
            description: req.body.description,
            yearOfCreation: req.body.yearOfCreation
        })
        await a.save();
        return res.status(200).json(`El album ${a.name} ha sigo guardado`);
    } catch {
        res.status(404).json(`El album no ha podido ser cargado`);
    }
})

albumRouter.put("/albums/:id", async (req, res) => {
    try {
        const a = await getModel('Album').findOne({
            where: {
                id: req.params.id
            }
        });
        a.description = req.body.description;
        a.save();
        res.status(200).json(b);
    } catch {
        res.status(404).json(`No hay albumes`);
    }
})

albumRouter.delete("/albums/:id", async (req, res) => {
    try {
        const a = await getModel('Album').findOne({
            where: {
                id: req.params.id
            }
        });
        a.destroy();
        res.status(200).json(`El album ${b.name} ha sido eliminado` );
    } catch {
        res.status(404).json(`No hay albumes`);
    }
})

module.exports = {
    albumRouter
}