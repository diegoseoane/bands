const express = require("express");
const { getModel } = require("../database/index.js");
const bandRouter = express();

bandRouter.get("/bands", async (req, res) => {
    try {
        const b = await getModel('Band').findAll();
        res.status(200).json(b);
    } catch {
        res.status(404).json(`No hay bandas`);
    }
})

bandRouter.get("/bands/:id", async (req, res) => {
    try {
        const Album = await getModel('Album');
        const b = await getModel('Band').findOne({
            where: {
                id: req.params.id
            },
            include: Album
        });
        res.status(200).json(b);
    } catch {
        res.status(404).json(`No hay bandas`);
    }
})

bandRouter.post("/bands", async (req, res) => {
    try {
        const Band = getModel('Band');
        const b = new Band({
            name: req.body.name,
            description: req.body.description,
            yearOfCreation: req.body.yearOfCreation
        })
        await b.save();
        return res.status(200).json(`La banda ${b.name} ha sigo guardada`);
    } catch {
        res.status(404).json(`La banda no ha podido ser cargada`);
    }
})

bandRouter.put("/bands/:id", async (req, res) => {
    try {
        const b = await getModel('Band').findOne({
            where: {
                id: req.params.id
            }
        });
        b.description = req.body.description;
        b.save();
        res.status(200).json(b);
    } catch {
        res.status(404).json(`No hay bandas`);
    }
})

bandRouter.delete("/bands/:id", async (req, res) => {
    try {
        const b = await getModel('Band').findOne({
            where: {
                id: req.params.id
            }
        });
        b.destroy();
        res.status(200).json(`La banda ha sido eliminada` );
    } catch {
        res.status(404).json(`No hay bandas`);
    }
})

module.exports = {
    bandRouter
}